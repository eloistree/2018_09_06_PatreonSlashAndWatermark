﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformActivator : MonoBehaviour {

    public RuntimePlatform m_current;
    public PlatformToObject[] activators;
    [System.Serializable]
    public class PlatformToObject
    {
        public RuntimePlatform [] m_platform;
        public GameObject [] m_objects;
        public void ActivateAll() {
            for (int i = 0; i < m_objects.Length; i++)
            {
                m_objects[i].SetActive(true);
            }
        }
        public bool IsOnCorresponding() {

            for (int i = 0; i < m_platform.Length; i++)
            {
                if (Application.platform == m_platform[i])
                    return true;
            }
            return false;
        }
    }
    
	void Awake () {
        for (int i = 0; i < activators.Length; i++)
        {
            if (activators[i].IsOnCorresponding())
                activators[i].ActivateAll();
        }
		
	}
	
}
