﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RatioPanelSwitcher : MonoBehaviour {

    public RatioType m_ratio = RatioType.Unkown;
    public enum RatioType { Vertical, Horizontal, Unkown}
    public GameObject[] m_vertical;
    public GameObject[] m_horizontal;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        RatioType ratio = Screen.width / Screen.height < 1f ? RatioType.Vertical : RatioType.Horizontal;
        if (m_ratio != ratio) {
            bool horizontal = (ratio == RatioType.Horizontal);

            for (int i = 0; i < m_horizontal.Length; i++)
            {
                m_horizontal[i].SetActive(horizontal);
            }
            for (int i = 0; i < m_vertical.Length; i++)
            {
                m_vertical[i].SetActive(!horizontal);
            }

        }

		
	}
}
